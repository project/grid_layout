Grid Layout
============

DESCRIPTION
-----------
Grid Layout is a simple module that provides a layout plugin which can dynamically define
new regions using css grid template columns and areas.

REQUIREMENTS
------------
This module requires Layout Discovery modules of Drupal core.

INSTALLATION
--------------
 * Install the Grid Layout module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

MAINTAINERS
------------
 * Kris Vanderwater (EclipseGc) - https://www.drupal.org/u/eclipsegc

