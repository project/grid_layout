<?php

namespace Drupal\grid_layout\EventSubscriber;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\grid_layout\Event\GridLayoutFormBuildEvent;
use Drupal\grid_layout\Event\GridLayoutFormSubmitEvent;
use Drupal\grid_layout\Event\GridLayoutFormValidateEvent;
use Drupal\grid_layout\GridLayoutEvents;
use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GridLayoutFormSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The Drupal file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The library discovery.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The uuid generator.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * GridLayoutFormSubscriber constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The Drupal file system
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   *   The library discovery.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid generator.
   */
  public function __construct(FileSystemInterface $fileSystem, LibraryDiscoveryInterface $libraryDiscovery, UuidInterface $uuid) {
    $this->fileSystem = $fileSystem;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[GridLayoutEvents::FORM_BUILD][] = 'onFormBuild';
    $events[GridLayoutEvents::FORM_VALIDATE][] = 'onFormValidate';
    $events[GridLayoutEvents::FORM_SUBMIT][] = 'onFormSubmit';
    return $events;
  }

  /**
   * @param \Drupal\grid_layout\Event\GridLayoutFormBuildEvent $event
   */
  public function onFormBuild(GridLayoutFormBuildEvent $event) {
    $layout = $event->getLayout();
    $form = $event->getForm();
    $form['grid_template_columns'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Grid template columns'),
      '#description' => $this->t('Follow the css standard defined for the grid-template-columns css property.'),
      '#default_value' => $layout->getConfiguration()['grid_template_columns'],
    ];
    $form['grid_template_areas'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Grid template areas'),
      '#description' => $this->t('Follow the css standard defined for the grid-template-areas css property.'),
      '#default_value' => $layout->getConfiguration()['grid_template_areas'],
    ];
    $event->setForm($form);
  }

  public function onFormValidate(GridLayoutFormValidateEvent $event) {
    $regions = [];
    $column_widths = explode(' ', $event->getFormState()->getValue('grid_template_columns'));
    $supported_suffixes = [
      'cm',
      'mm',
      'in',
      'px',
      'pt',
      'pc',
      'em',
      'ex',
      'ch',
      'rem',
      'vw',
      'vh',
      'vmin',
      'vmax',
      '%',
      'fr',
      'none',
      'auto',
      'max-content',
      'min-content',
      'initial',
      'inherit',
    ];
    foreach ($column_widths as $width) {
      foreach ($supported_suffixes as $suffix) {
        if (strpos($width, $suffix) !== FALSE) {
          continue 2;
        }
      }
      $event->getFormState()->setErrorByName('grid_template_columns', $this->t("The grid template columns only support widths of the following formats: @formats", ['@formats' => implode(', ', $supported_suffixes)]));
    }
    foreach (explode("\n", $event->getFormState()->getValue('grid_template_areas')) as $row) {
      $columns = explode(' ', rtrim($row));
      if (count($columns) !== count($column_widths)) {
        $event->getFormState()->setErrorByName('grid_template_areas', $this->t("The grid template areas do not have the same number of columns as specified in the grid template columns. Each row of areas must have the same number of columns as the grid template columns element. Please resolve this error and resubmit."));
      }
      foreach ($columns as $index => $column) {
        if (isset($regions[$column]) || $column === '.') {
          continue;
        }
        $regions[$column] = [
          'column' => $column,
          'name' => $column,
          'width' => $column_widths[$index],
        ];
      }
    }

    $event->getFormState()->set('regions', $regions);
  }

  public function onFormSubmit(GridLayoutFormSubmitEvent $event) {
    $configuration = $event->getLayout()->getConfiguration();
    $form_state = $event->getFormState();
    $configuration['grid_template_columns'] = $form_state->getValue('grid_template_columns');
    $configuration['grid_template_areas'] = $form_state->getValue('grid_template_areas');
    $configuration['columns'] = $form_state->get('regions');

    $dir = "public://grid_layout";
    $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

    /** @var SectionStorageInterface $section_storage */
    $section_storage = $form_state->getBuildInfo()['args'][0];
    $file_name = $configuration['grid_css_file_name'] ?? $this->getFileName($section_storage);
    [, $uuid, $storage_id, ] = explode('__', $file_name);
    // Check whether this is a new section storage type.
    if ($storage_id === $section_storage->getPluginId()) {
      // Delete our old file if this is an update.
      $this->fileSystem->delete("$dir/$file_name.css");
    }
    else {
      // Otherwise, remove the uuid we've got in so a new one generates.
      $uuid = NULL;
    }

    $file_name = $this->getFileName($form_state->getBuildInfo()['args'][0], $uuid);
    $css = $this->getCss($event->getLayout(), $configuration, $file_name);
    $file_location = $this->fileSystem->saveData($css, "$dir/$file_name.css", FileSystemInterface::EXISTS_REPLACE);

    $configuration['grid_css_file_name'] = $file_name;
    $configuration['grid_css_file_location'] = $file_location;
    $event->getLayout()->setConfiguration($configuration);
    // Rebuild libraries.
    $this->libraryDiscovery->clearCachedDefinitions();
  }

  protected function getFileName(SectionStorageInterface $section_storage, string $uuid = NULL) : string {
    $uuid = $uuid ?? $this->uuid->generate();
    $storage_id = $section_storage->getPluginId();
    return "grid_layout__{$uuid}__{$storage_id}__" . time();
  }

  protected function getCss(GridLayout $layout, array $configuration, string $container) : string {
    $area = "\"" . implode("\"\n                       \"", explode("\r\n", $configuration['grid_template_areas'])) . "\"";
    $css = ".$container {
  display: grid;
  grid-template-areas: $area;
  grid-template-columns: {$configuration['grid_template_columns']};
}\n\n";
    foreach ($configuration['columns'] as $child => $data) {
      $css .= ".$container > .grid-$child {
  grid-area: $child;
}\n\n";
    }
    return $css;
  }

}
