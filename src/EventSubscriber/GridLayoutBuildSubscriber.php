<?php

namespace Drupal\grid_layout\EventSubscriber;

use Drupal\grid_layout\Event\GridLayoutBuildEvent;
use Drupal\grid_layout\Event\GridLayoutRegionsAlterEvent;
use Drupal\grid_layout\GridLayoutEvents;
use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GridLayoutBuildSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[GridLayoutEvents::REGIONS_ALTER][] = 'onRegionsAlter';
    $events[GridLayoutEvents::BUILD_LAYOUT][] = 'onBuild';
    return $events;
  }

  public function onRegionsAlter(GridLayoutRegionsAlterEvent $event) {
    $layout = $event->getLayout();
    $configuration = $layout->getConfiguration();
    $regions = $event->getRegions();

    $region_settings = [];
    foreach ($configuration['columns'] as $region) {
      $region_settings[$region['column']]['label'] = $region['name'];
      if (!isset($regions[$region['column']])) {
        $regions[$region['column']] = [];
      }
    }
    $layout->getPluginDefinition()->setRegions($region_settings);
    $layout->getPluginDefinition()->setLibrary("grid_layout/{$configuration['grid_css_file_name']}");
  }

  public function onBuild(GridLayoutBuildEvent $event) {
    $layout = $event->getLayout();
    $configuration = $layout->getConfiguration();
    $build = $event->getBuild();

    foreach ($configuration['columns'] as $child => $data) {
      $build[$child]['#attributes']['class'][] = "grid-$child";
    }
    $build['#attributes']['class'][] = $this->getClassName($layout);
    $build['#attributes']['class'][] = $configuration['grid_css_file_name'];
    $event->setBuild($build);
  }

  protected function getClassName(GridLayout $layout) {
    $configuration = $layout->getConfiguration();
    [$prefix, $uuid] = explode('__', $configuration['grid_css_file_name']);
    return "{$prefix}__{$uuid}";
  }

}
