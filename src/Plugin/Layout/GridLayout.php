<?php

namespace Drupal\grid_layout\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\grid_layout\Event\GridLayoutBuildEvent;
use Drupal\grid_layout\Event\GridLayoutDefaultConfigEvent;
use Drupal\grid_layout\Event\GridLayoutFormBuildEvent;
use Drupal\grid_layout\Event\GridLayoutFormSubmitEvent;
use Drupal\grid_layout\Event\GridLayoutFormValidateEvent;
use Drupal\grid_layout\Event\GridLayoutRegionsAlterEvent;
use Drupal\grid_layout\GridLayoutEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class GridLayout extends LayoutDefault implements PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * The event dispatcher
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $dispatcher;

  /**
   * GridLayout constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $dispatcher) {
    // The parent calls ::setConfiguration() which proxies to
    // ::defaultConfiguration(), so we have to setup the dispatcher before
    // calling parent::__construct().
    $this->dispatcher = $dispatcher;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = parent::defaultConfiguration() + [
      'grid_template_columns' => '',
      'grid_template_areas' => '',
      'columns' => [],
    ];
    $event = new GridLayoutDefaultConfigEvent($this, $default_config);
    $this->dispatcher->dispatch(GridLayoutEvents::DEFAULT_CONFIG, $event);
    return $event->getDefaultConfig();
  }

  /**
   * @inheritDoc
   */
  public function build(array $regions) {
    $event = new GridLayoutRegionsAlterEvent($this, $regions);
    $this->dispatcher->dispatch(GridLayoutEvents::REGIONS_ALTER, $event);
    $regions = $event->getRegions();

    $build = parent::build($regions);

    $event = new GridLayoutBuildEvent($this, $build, $regions);
    $this->dispatcher->dispatch(GridLayoutEvents::BUILD_LAYOUT, $event);
    return $event->getBuild();
  }


  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $event = new GridLayoutFormBuildEvent($this, $form, $form_state);
    $this->dispatcher->dispatch(GridLayoutEvents::FORM_BUILD, $event);
    return $event->getForm();
  }

  /**
   * @inheritDoc
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $event = new GridLayoutFormValidateEvent($this, $form, $form_state);
    $this->dispatcher->dispatch(GridLayoutEvents::FORM_VALIDATE, $event);
    $form = $event->getForm();
    $form_state = $event->getFormState();
  }

  /**
   * @inheritDoc
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $event = new GridLayoutFormSubmitEvent($this, $form, $form_state);
    $this->dispatcher->dispatch(GridLayoutEvents::FORM_SUBMIT, $event);
    $form = $event->getForm();
    $form_state = $event->getFormState();
  }

}
