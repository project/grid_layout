<?php

namespace Drupal\grid_layout;

final class GridLayoutEvents {

  /**
   * The event to determine default configuration of the grid layout plugin.
   *
   * @see \Drupal\grid_layout\Event\GridLayoutDefaultConfigEvent
   */
  const DEFAULT_CONFIG = 'grid_layout_default_config';

  /**
   * The event to process regions before calling LayoutDefault::build().
   *
   * @see \Drupal\grid_layout\Event\GridLayoutRegionsAlterEvent
   */
  const REGIONS_ALTER = 'grid_layout_regions_alter';

  /**
   * The event that builds the grid layout's render array.
   *
   * @see \Drupal\grid_layout\Event\GridLayoutBuildEvent
   */
  const BUILD_LAYOUT = 'grid_layout_build_layout';

  /**
   * The event that builds a grid layout configuration form.
   *
   * @see \Drupal\grid_layout\Event\GridLayoutFormBuildEvent
   */
  const FORM_BUILD = 'grid_layout_form_build';

  /**
   * The event that validates a grid layout form.
   *
   * @see \Drupal\grid_layout\Event\GridLayoutFormValidateEvent
   */
  const FORM_VALIDATE = 'grid_layout_form_validate';

  /**
   * The event that submits a grid layout form.
   *
   * @see \Drupal\grid_layout\Event\GridLayoutFormSubmitEvent
   */
  const FORM_SUBMIT = 'grid_layout_form_submit';

}
