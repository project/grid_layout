<?php

namespace Drupal\grid_layout\Event;

use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Symfony\Component\EventDispatcher\Event;

class GridLayoutRegionsAlterEvent extends Event {

  /**
   * @var \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  protected $layout;

  /**
   * @var array
   */
  protected $regions;

  /**
   * GridLayoutPreBuildEvent constructor.
   *
   * @param \Drupal\grid_layout\Plugin\Layout\GridLayout $layout
   * @param array $regions
   */
  public function __construct(GridLayout $layout, array $regions) {
    $this->layout = $layout;
    $this->regions = $regions;
  }

  /**
   * @return \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  public function getLayout(): \Drupal\grid_layout\Plugin\Layout\GridLayout {
    return $this->layout;
  }

  /**
   * @return array
   */
  public function getRegions(): array {
    return $this->regions;
  }

  /**
   * @param array $regions
   */
  public function setRegions(array $regions): void {
    $this->regions = $regions;
  }

}
