<?php

namespace Drupal\grid_layout\Event;

use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Symfony\Component\EventDispatcher\Event;

class GridLayoutDefaultConfigEvent extends Event {

  /**
   * The grid layout plugin.
   *
   * @var \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  protected $layout;

  /**
   * The default configuration to return.
   *
   * @var array
   */
  protected $defaultConfig;

  /**
   * GridLayoutDefaultConfigEvent constructor.
   *
   * @param \Drupal\grid_layout\Plugin\Layout\GridLayout $layout
   */
  public function __construct(GridLayout $layout, array $default_config) {
    $this->layout = $layout;
    $this->defaultConfig = $default_config;
  }

  /**
   * Get the current grid layout.
   *
   * @return \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  public function getLayout() : GridLayout {
    return $this->layout;
  }

  /**
   * Get the default configuration.
   *
   * @return array
   */
  public function getDefaultConfig() : array {
    return $this->defaultConfig;
  }

  /**
   * Set the default configuration.
   *
   * @param array $config
   *   The configuration to set as default.
   */
  public function setDefaultConfig(array $config) : void {
    $this->defaultConfig = $config;
  }

}
