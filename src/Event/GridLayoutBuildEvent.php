<?php

namespace Drupal\grid_layout\Event;

use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Symfony\Component\EventDispatcher\Event;

class GridLayoutBuildEvent extends Event {

  /**
   * The grid layout plugin.
   *
   * @var \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  protected $layout;

  /**
   * The build array to be returned.
   *
   * @var array
   */
  protected $build;

  /**
   * The array of regions with data.
   *
   * @var array
   */
  protected $regions;

  /**
   * GridLayoutBuildEvent constructor.
   *
   * @param \Drupal\grid_layout\Plugin\Layout\GridLayout $layout
   * @param array $build
   * @param array $regions
   */
  public function __construct(GridLayout $layout, array $build, array $regions) {
    $this->layout = $layout;
    $this->build = $build;
    $this->regions = $regions;
  }

  /**
   * Get the current grid layout.
   *
   * @return \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  public function getLayout(): GridLayout {
    return $this->layout;
  }

  /**
   * Get the render array for this grid layout.
   *
   * @return array
   */
  public function getBuild(): array {
    return $this->build;
  }

  /**
   * Se the render array for this grid layout.
   *
   * @param array $build
   */
  public function setBuild(array $build): void {
    $this->build = $build;
  }

  /**
   * Get the regions to be used for this grid layout.
   *
   * @return array
   */
  public function getRegions(): array {
    return $this->regions;
  }

}
