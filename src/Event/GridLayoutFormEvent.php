<?php

namespace Drupal\grid_layout\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\grid_layout\Plugin\Layout\GridLayout;
use Symfony\Component\EventDispatcher\Event;

abstract class GridLayoutFormEvent extends Event {

  /**
   * The grid layout plugin.
   *
   * @var \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  protected $layout;

  /**
   * The form array.
   *
   * @var array
   */
  protected $form;

  /**
   * The FormState object.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * GridLayoutFormBuildEvent constructor.
   *
   * @param \Drupal\grid_layout\Plugin\Layout\GridLayout $layout
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function __construct(GridLayout $layout, array $form, FormStateInterface $form_state) {
    $this->layout = $layout;
    $this->form = $form;
    $this->formState = $form_state;
  }

  /**
   * Get the grid layout plugin.
   *
   * @return \Drupal\grid_layout\Plugin\Layout\GridLayout
   */
  public function getLayout(): GridLayout {
    return $this->layout;
  }

  /**
   * Get the form array.
   *
   * @return array
   */
  public function getForm(): array {
    return $this->form;
  }

  /**
   * Set the form array.
   *
   * @param array $form
   */
  public function setForm(array $form): void {
    $this->form = $form;
  }

  /**
   * Get the FormState object.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

}
